package com.example.boarddocumentpaging.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BoardCommentCreateRequest {
    @ApiModelProperty(notes = "작성자 (2~20)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String commentUserName;
    @ApiModelProperty(notes = "댓글 내용 (2~)" , required = true)
    @NotNull
    @Length(min = 2)
    private String commentContents;

}
