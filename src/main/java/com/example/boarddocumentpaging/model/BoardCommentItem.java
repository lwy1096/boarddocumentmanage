package com.example.boarddocumentpaging.model;

import com.example.boarddocumentpaging.entity.BoardComment;
import com.example.boarddocumentpaging.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardCommentItem {

    private Long boardId;
    private Long commentId;
    private String commentUserName;
    private String commentContents;
    private LocalDateTime commentCreateDate;

    private BoardCommentItem(BoardCommentItemBuilder builder) {
        this.boardId = builder.boardId;
        this.commentId = builder.commentId;
        this.commentUserName = builder.commentUserName;
        this.commentContents = builder.commentContents;
        this.commentCreateDate = builder.commentCreateDate;
    }
    public static class BoardCommentItemBuilder implements CommonModelBuilder<BoardCommentItem> {
        private final Long boardId;
        private final Long commentId;
        private final String commentUserName;
        private final String commentContents;
        private final LocalDateTime commentCreateDate;

        public BoardCommentItemBuilder(BoardComment boardComment) {
            this.boardId = boardComment.getBoardId();
            this.commentId = boardComment.getId();
            this.commentUserName = boardComment.getCommentUserName();
            this.commentContents = boardComment.getCommentContents();
            this.commentCreateDate = LocalDateTime.now();
        }

        @Override
        public BoardCommentItem build() {
            return new BoardCommentItem(this);
        }
    }
}
