package com.example.boarddocumentpaging.model;

import com.example.boarddocumentpaging.entity.BoardDocument;
import com.example.boarddocumentpaging.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardDocumentCreatedItem {

    @ApiModelProperty(notes = "id 시퀀스값")
    private Long id;
    @ApiModelProperty(notes = "게시글 제목")
    private String title;
    @ApiModelProperty(notes = "작성자")
    private String userName;
    @ApiModelProperty(notes = "작성일")
    private LocalDateTime createDate;
    @ApiModelProperty(notes = "조회수")
    private Integer viewCount;


    private BoardDocumentCreatedItem(BoardDocumentCreatedItemBuilder builder) {
        this.id = builder.id;
        this.title = builder.title;
        this.userName = builder.userName;
        this.createDate = builder.createDate;
        this.viewCount = builder.viewCount;
    }

    public static class BoardDocumentCreatedItemBuilder implements CommonModelBuilder<BoardDocumentCreatedItem> {
        private final Long id;
        private final String title;
        private final String userName;
        private final LocalDateTime createDate;
        private final Integer viewCount;

        public BoardDocumentCreatedItemBuilder(BoardDocument boardDocument) {
            this.id = boardDocument.getId();
            this.title = boardDocument.getTitle();
            this.userName = boardDocument.getUserName();
            this.createDate = boardDocument.getCreateDate();
            this.viewCount = boardDocument.getViewCount();

        }
        @Override
        public BoardDocumentCreatedItem build() {
            return new BoardDocumentCreatedItem(this);
        }
    }
}
