package com.example.boarddocumentpaging.repository;

import com.example.boarddocumentpaging.entity.BoardDocument;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardDocumentRepository extends JpaRepository<BoardDocument, Long> {
}
