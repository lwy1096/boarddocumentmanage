package com.example.boarddocumentpaging.repository;

import com.example.boarddocumentpaging.entity.BoardComment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BoardCommentCreateRepository extends JpaRepository<BoardComment,Long> {
    Page<BoardComment> findAllByBoardIdOrderByIdDesc(long boardDocumentId, Pageable pageable);
    //Page<BoardComment>의 값에서 보드의 아이디값을 바탕으로 페이지 리스트를 만들도록 함 (*게시글 id에 해당하는 댓글만 추려서 리스트로 변경)

}
