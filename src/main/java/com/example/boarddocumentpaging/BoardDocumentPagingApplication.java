package com.example.boarddocumentpaging;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BoardDocumentPagingApplication {

	public static void main(String[] args) {
		SpringApplication.run(BoardDocumentPagingApplication.class, args);
	}

}
