package com.example.boarddocumentpaging.service;

import com.example.boarddocumentpaging.entity.BoardDocument;
import com.example.boarddocumentpaging.model.BoardDocumentCreateRequest;
import com.example.boarddocumentpaging.model.BoardDocumentCreatedItem;
import com.example.boarddocumentpaging.model.ListResult;
import com.example.boarddocumentpaging.repository.BoardDocumentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardDocumentService {
    private final BoardDocumentRepository boardDocumentRepository;

    public void setDocument(BoardDocumentCreateRequest request) {
        BoardDocument originData = new BoardDocument.BoardDocumentBuilder(request).build();

        boardDocumentRepository.save(originData);
    }

    public ListResult<BoardDocumentCreatedItem> getDocuments(int pageNum) {
        Page<BoardDocument> originList = boardDocumentRepository.findAll(ListConvertService.getPageable(pageNum));
        //ListConvertService.getPageable(원하는 페이지 번호)
        List<BoardDocumentCreatedItem> result = new LinkedList<>();
        for(BoardDocument item : originList.getContent()) {
            //originList에서 리스트만 가져와야하기 때문에 originList.getContent() 로 작성.
            result.add(new BoardDocumentCreatedItem.BoardDocumentCreatedItemBuilder(item).build());
        }
        return ListConvertService.settingResult(
                result,//리스트
                originList.getTotalElements(),//원본의 갯수
                originList.getTotalPages(), //페이지의 갯수
                originList.getPageable().getPageNumber() // 현재 페이지
        );

    }
}
