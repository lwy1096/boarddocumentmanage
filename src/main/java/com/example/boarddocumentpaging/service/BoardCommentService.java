package com.example.boarddocumentpaging.service;

import com.example.boarddocumentpaging.entity.BoardComment;
import com.example.boarddocumentpaging.entity.BoardDocument;
import com.example.boarddocumentpaging.exception.CMissingDataException;
import com.example.boarddocumentpaging.model.BoardCommentCreateRequest;
import com.example.boarddocumentpaging.model.BoardCommentItem;
import com.example.boarddocumentpaging.model.BoardDocumentCreateRequest;
import com.example.boarddocumentpaging.model.ListResult;
import com.example.boarddocumentpaging.repository.BoardCommentCreateRepository;
import com.example.boarddocumentpaging.repository.BoardDocumentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardCommentService {
    private final BoardCommentCreateRepository boardCommentCreateRepository;
    private final BoardDocumentRepository boardDocumentRepository;

    public void setComment(long id, BoardCommentCreateRequest request) {
        BoardDocument mainData = boardDocumentRepository.findById(id).orElseThrow(CMissingDataException::new);
        BoardComment commentData = new BoardComment.BoardCommentBuilder(id,request).build();

        boardCommentCreateRepository.save(commentData);
    }
    public ListResult<BoardCommentItem> getComments(long id, int pageNum) {
        Page<BoardComment> originList = boardCommentCreateRepository.findAllByBoardIdOrderByIdDesc(id, ListConvertService.getPageable(pageNum,15    ));
        List<BoardCommentItem> result = new LinkedList<>();
        for(BoardComment item : originList.getContent()) {
            result.add( new BoardCommentItem.BoardCommentItemBuilder(item).build());
        }
        return ListConvertService.settingResult(
                result, // 오리지널 리스트값
                originList.getTotalElements(), // 원본의 갯수
                originList.getTotalPages(), // 총 페이지
                originList.getPageable().getPageNumber() // 페이지 번호
        );
    }
}
