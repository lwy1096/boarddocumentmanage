package com.example.boarddocumentpaging.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}

