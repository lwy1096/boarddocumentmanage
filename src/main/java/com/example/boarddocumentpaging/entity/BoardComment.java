package com.example.boarddocumentpaging.entity;

import com.example.boarddocumentpaging.interfaces.CommonModelBuilder;
import com.example.boarddocumentpaging.model.BoardCommentCreateRequest;
import com.example.boarddocumentpaging.model.BoardDocumentCreateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardComment {
    @ApiModelProperty(notes = "ID 시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "게시글 ID")
    @Column(nullable = false)
    private Long boardId;

    @ApiModelProperty(notes = "댓글 작성자")
    @Column(nullable = false, length = 20)
    private String commentUserName;
    @ApiModelProperty(notes = "댓글 내용")
    @Column(nullable = false, columnDefinition = "TEXT")
    private String commentContents;

    @ApiModelProperty(notes = "댓글 작성일")
    @Column(nullable = false)
    private LocalDateTime commentCreateDate;

    private BoardComment(BoardCommentBuilder builder) {
        this.boardId = builder.boardId;
        this.commentUserName = builder.commentUserName;
        this.commentContents = builder.commentContents;
        this.commentCreateDate = builder.commentCreateDate;

    }

    public static class BoardCommentBuilder implements CommonModelBuilder<BoardComment> {
        private final Long boardId;
        private final String commentUserName;
        private final String commentContents;
        private final LocalDateTime commentCreateDate;

        public BoardCommentBuilder(Long boardId, BoardCommentCreateRequest createRequest) {
            this.boardId = boardId;
            this.commentCreateDate = LocalDateTime.now();
            this.commentUserName= createRequest.getCommentUserName();
            this.commentContents = createRequest.getCommentContents();

        }

        @Override
        public BoardComment build() {
            return new BoardComment(this);
        }
    }

}
