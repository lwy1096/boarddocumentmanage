package com.example.boarddocumentpaging.entity;

import com.example.boarddocumentpaging.interfaces.CommonModelBuilder;
import com.example.boarddocumentpaging.model.BoardDocumentCreateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardDocument {
    @ApiModelProperty(notes = "id 시퀀스값")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "게시글 제목")
    @Column(nullable = false, length = 100)
    private String title;

    @ApiModelProperty(notes = "게시글 내용")
    @Column(nullable = false, columnDefinition = "TEXT")
    private String contents;

    @ApiModelProperty(notes = "작성자")
    @Column(nullable = false, length = 20)
    private String userName;

    @ApiModelProperty(notes = "작성일")
    private LocalDateTime createDate;

    @ApiModelProperty(notes = "조회수")
    private Integer viewCount;

    private BoardDocument(BoardDocumentBuilder builder) {
        this.title = builder.title;
        this.contents = builder.contents;
        this.userName = builder.userName;
        this.createDate = builder.createDate;
        this.viewCount = builder.viewCount;

    }

    public static class BoardDocumentBuilder implements CommonModelBuilder<BoardDocument> {

        private final String title;
        private final String contents;
        private final String userName;
        private final LocalDateTime createDate;
        private final Integer viewCount;

        public BoardDocumentBuilder(BoardDocumentCreateRequest createRequest) {
            this.title = createRequest.getTitle();
            this.contents = createRequest.getCommentContents();
            this.userName = createRequest.getCommentUserName();
            this.createDate = LocalDateTime.now();
            this.viewCount = 0;
        }

        @Override
        public BoardDocument build() {
            return new BoardDocument(this);
        }
    }



}
