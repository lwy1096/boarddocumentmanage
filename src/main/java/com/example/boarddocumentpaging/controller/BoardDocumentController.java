package com.example.boarddocumentpaging.controller;

import com.example.boarddocumentpaging.model.BoardDocumentCreateRequest;
import com.example.boarddocumentpaging.model.BoardDocumentCreatedItem;
import com.example.boarddocumentpaging.model.CommonResult;
import com.example.boarddocumentpaging.model.ListResult;
import com.example.boarddocumentpaging.service.BoardDocumentService;
import com.example.boarddocumentpaging.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags="게시글 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board-document")
public class BoardDocumentController {
    private final BoardDocumentService boardDocumentService;
    @ApiOperation(value = "게시글 작성")
    @PostMapping("/new")
    public CommonResult setDocument(@RequestBody @Valid BoardDocumentCreateRequest request) {
        boardDocumentService.setDocument(request);
        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "게시글 리스트")
    @GetMapping("/all/page/{pageNum}")
    public ListResult<BoardDocumentCreatedItem> getDocuments(@PathVariable int pageNum) {
        return ResponseService.getListResult(boardDocumentService.getDocuments(pageNum),true);

    }
}
