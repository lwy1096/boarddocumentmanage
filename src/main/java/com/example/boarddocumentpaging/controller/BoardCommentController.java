package com.example.boarddocumentpaging.controller;

import com.example.boarddocumentpaging.model.BoardCommentCreateRequest;
import com.example.boarddocumentpaging.model.BoardCommentItem;
import com.example.boarddocumentpaging.model.CommonResult;
import com.example.boarddocumentpaging.model.ListResult;
import com.example.boarddocumentpaging.repository.BoardCommentCreateRepository;
import com.example.boarddocumentpaging.service.BoardCommentService;
import com.example.boarddocumentpaging.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "댓글달기")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board-comment")
public class BoardCommentController {
    private final BoardCommentService boardCommentService;
    @ApiOperation(value = "댓글등록")
    @PostMapping("/add-comment/{id}")
    public CommonResult setComment(@PathVariable long id, @RequestBody @Valid BoardCommentCreateRequest createRequest){
        boardCommentService.setComment(id,createRequest);
        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "댓글 리스트 보기")
    @GetMapping("/comment-list/{id}/{pageNum}")
    public ListResult<BoardCommentItem> getComments (@PathVariable int pageNum, @PathVariable long id) {
        return ResponseService.getListResult(boardCommentService.getComments(id,pageNum),true);
    }
}
